class Etalon {
    constructor(arrText) {
        this.arrText = arrText;
        this.stringText;
    }

    delSymbols() {
        var arr = this.arrText;

        for(var i = 0; i < arr.length; i++) {
            arr[i] = arr[i].toLowerCase();
            for(var j = 0; j<arr[i].length; j++) {
                if ( (arr[i][j].charCodeAt(0) < 'а'.charCodeAt(0) || arr[i][j].charCodeAt(0) > 'я'.charCodeAt(0))) {
                    arr[i] = arr[i].replace(arr[i][j], '');
                    j--;
                }
            }
            if(arr[i] == ''){
                arr.splice(i, 1);
                i--;
            }
        }

        this.arrText = arr.slice();
        return this.arrText;
    }

    toStr(){
        this.stringText = this.arrText.join('').substr(0);
        return this.stringText;
    };

    keyBuild() {
        let key = document.getElementById("key").value.split(' ').join('');
        if(!key) alert("Пожалуйста, введите ключ");

        return this.key = key;
    }

    chiper() {
        let text = this.stringText,
            key = this.key,
            vizhiner = vizhinerTable();
        let chiperText = [];

        for(let i = 0; i < text.length; i++) {
            let keyLetter;
            let posI, posJ;

            if(key[i] === undefined) {
                keyLetter = key[i % key.length]
            } else {
                keyLetter = key[i];
            }

            for(let s = 0; s < 33; s++) {
                if (vizhiner[0][s].indexOf(text[i]) === 0) posI = s;
                if (vizhiner[0][s].indexOf(keyLetter) === 0) posJ = s;
            }

            chiperText[i] = vizhiner[posI][posJ]
        }

        return this.chiperText = chiperText.join('');
    }
    dechiper(key) {
        let text = this.chiperText,
            vizhiner = vizhinerTable();

        let dechiperText = [];

        for(let i = 0; i < text.length; i++) {
            let keyLetter;
            let posI, posJ;

            if(key[i] === undefined) {
                keyLetter = key[i % key.length]
            } else {
                keyLetter = key[i];
            }

            for(let s = 0; s < 33; s++) {
                if (vizhiner[0][s].indexOf(keyLetter) === 0){
                    posJ = s;
                    for(let q = 1; q < 33; q++) {
                        if( vizhiner[s][q].indexOf(text[i]) === 0){
                            posI = q;
                        }
                    }
                }
            }

            dechiperText[i] = vizhiner[0][posI]
        }

        return this.dechiperText = dechiperText.join('');
    }
    encrypt() {
        let text = this.chiperText.split('');
        let coincidenceIndex = 0.0553;

        let keyLen = keyLenDetermination();
        let parts = makeParts(keyLen.keyLen);
        let key = keyDetermination(parts);

        console.log(keyLen)
        console.log(parts)
        console.log(key)




        function textPartMade(estimatedKeyLen, text) {
            let textPart = '';
            for(let i = estimatedKeyLen-1; i < text.length; i++) {
                if((i+1) % estimatedKeyLen == 0) textPart += text[i];
            }
            return textPart;
        }

        function keyLenDetermination() {
            let minDifference = {
                estimatedIndex: [],
                diff: [],
                keyLen: []
            }

            for(let iteration = 2; iteration < 10; iteration++) {
                let textPart = textPartMade(iteration, text)
                let soloGrams = findnGrams(textPart, 1)

                let estimatedIndex = 0;

                for(let key in soloGrams) {
                    soloGrams[key] = (soloGrams[key] * soloGrams[key]-1)/(textPart.length*textPart.length-1);

                    estimatedIndex += soloGrams[key];
                }

                let difference = Math.abs(estimatedIndex - coincidenceIndex)
                if(difference < 0.002) {
                    minDifference.estimatedIndex.push(estimatedIndex);
                    minDifference.diff.push(difference);
                    minDifference.keyLen.push(iteration);
                }

                console.log('i: ' + iteration, 'ИС: ' + estimatedIndex);
            }

            return minDifference
        }

        function makeParts(keyLen) {
            let parts = [];

            keyLen.forEach(function(item) {
                let part = new Array(item);

                for(let i = 0; i < item; i++) {
                    part[i] = '';

                    for(let j = i; j < text.length; j += item) {
                        part[i] += text[j];
                    }
                }

                parts.push(part);
            })

            return parts
        }

        function keyDetermination(parts) {
            let rightKey = [];
            parts.forEach(function(part) {
                let rightItemKey = []

                console.log(part);
                for(let i = 0; i < part.length; i++) {
                    let frequency = lettersFrequency(part[i]);
                    let letter = {
                        char: '',
                        value: 0
                    }

                    console.log(frequency);
                    for(let key in frequency){
                        if(frequency[key] > letter.value) {
                            letter.char = key;
                            letter.value = frequency[key];
                        }
                    }

                    let keyLetter = ('о'.charCodeAt(0) - letter.char.charCodeAt(0) + 32 ) % 32;
                    keyLetter = String.fromCharCode('я'.charCodeAt(0) - keyLetter);

                    rightItemKey.push(keyLetter);
                }
                rightKey.push(rightItemKey.join(''))
            })

            return rightKey;
        }
    }
}