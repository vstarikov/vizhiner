function frequency(arr) {
    var letters = arr.join('').substr(0)
    var frequency = {};
    for(var i = 0; i < letters.length; i++) {

        var fr = letters[i].toLowerCase();
        if(fr in frequency) {
            frequency[fr]++
        } else {
            frequency[fr] = 0;
            frequency[fr]++
        }
    }
    for (var key in frequency) {
        frequency[key] = Math.round( (frequency[key] / letters.length)*100000 ) / 100000
    }
    return frequency;
}



function coincidenceIndex(anon) {
    var letters = anon.join('').substr(0)
    var frequency = {};
    var count = {}
    for(var i = 0; i < letters.length; i++) {

        var fr = letters[i].toLowerCase();
        if(fr in frequency) {
            frequency[fr]++
        } else {
            frequency[fr] = 0;
            frequency[fr]++
        }
    }
    for (var key in frequency) {
        /*console.log(frequency[key] + ': ' + key)*/
        count[key] = frequency[key]
        frequency[key] = Math.round( (frequency[key] / letters.length)*100000 ) / 100000
    }

    var sum = 0;
    for(var key in count) {
        sum += count[key]*(count[key] -1)/ letters.length / (letters.length -1)
    }

    sum = Math.round(sum *1000)/1000
    //console.log(sum);

    return sum
}

function getRandom(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function compare(a, b){
    return b - a;
}

function compareRandom(a, b) {
    return Math.random() - 0.5;
}

function getClass(obj) {
    return ({}.toString.call(obj).slice(8, -1))
}

function allConcidence(text, letter) {
    var position = 0,
        positionArray = [];

    while(position <= text.length) {
        var found = text.indexOf(letter, position);

        if (found == -1)  break;
        positionArray.push(found);
        position = found + 1;

    }
    return positionArray;
}

function find(array, value) {

  for (var i = 0; i < array.length; i++) {
    if (array[i] == value) return i;
  }

  return -1;
}

function vizhinerTable() {
    const abcLen = 33;
    let abc = [],
        vizhiner = [];

    for(let i = 0; i < 32; i++) {
        abc[i] = String.fromCharCode('а'.charCodeAt(0) + i);
    }



    for(var i = 0; i < abcLen; i++) {
        vizhiner[i] = [];
        for(var j = 0; j < abcLen; j++) {
            if(i == 0 && j == 0) { vizhiner[i][j] = '-' } else {
                let s = i + j-1;

                vizhiner[i][j] = abc[s];
                if(abc[s] === undefined)
                    vizhiner[i][j] = abc[s % (abcLen -1)];
            }
        }
    }

    return vizhiner
}

function findnGrams(text, n, frequency) {
    var arr = text.split('');

    var bigrams = {};
    for(let i = 0; i < arr.length - n+1; i++) {
        let bigr = '';
        for(let s = 0; s < n; s++) {
            bigr += arr[i+s]
        }
        if(bigr in bigrams) {
            bigrams[bigr]++;
        } else{
            bigrams[bigr] = 0;
            bigrams[bigr]++;
        }
    }
    if(frequency === true) {
        for(var key in bigrams) {
            bigrams[key] = bigrams[key] / arr.length
        }
    }

    return this.nGrams = bigrams;
}

function lettersFrequency(text) {
    var letters = text.split('');
    var frequency = {};
    for(var i = 0; i < letters.length; i++) {
        var fr = letters[i];
        if(fr in frequency) {
            frequency[fr]++
        } else {
            frequency[fr] = 0;
            frequency[fr]++
        }
    }
    /*for (var key in frequency) {
        frequency[key] = Math.round( (frequency[key] / letters.length)*100000 ) / 100000
    }*/
    return frequency;
}

function copyObj(obj) {
    let newObj = {};

    for(var key in obj) {
        newObj[key] = obj[key]
    }

    return newObj;
}